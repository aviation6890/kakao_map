package com.kakao.api.map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KakaoMapApi20210315Application {

    public static void main(String[] args) {
        SpringApplication.run(KakaoMapApi20210315Application.class, args);
    }

}
