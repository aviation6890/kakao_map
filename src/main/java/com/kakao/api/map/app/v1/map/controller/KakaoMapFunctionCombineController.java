package com.kakao.api.map.app.v1.map.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class KakaoMapFunctionCombineController {

    /**
     * 1. 지도 생성하기
     * 2. 지도 이동시키기
     * 3. 지도 레벨 바꾸기
     * 4. 지도 정보 얻어오기
     * 5. 지도에 컨트롤 올리기
     * 6. 지도에 사용자 컨트롤 올리기
     * 7. 지도 이동 막기
     * 8. 지도 확대 축소 막기
     * 9. 지도에 교통정보 표시하기
     * 10. 지도에 로드뷰 도로 표시하기
     * 11. 지도에 지형도 표시하기
     * 12. 지도 타입 바꾸기1
     * 13. 지도 타입 바꾸기2
     * 14. 지도 범위 재설정 하기
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/totalFunctionMap1")
    public String totalFunctionMap1(Model model) throws Exception {
        model.addAttribute("title", "1. 지도 기능 전부 때려박기.");
        log.info("model :: {}", model);
        return "map/totalFunctionMap1";
    }






    /**
     * 15. 지도 영역 크기 동적 변경하기
     * 16. 클릭 이벤트 등록하기
     * 17. 클릭한 위치에 마커 표시하기
     * 18. 이동 이벤트 등록하기
     * 19. 확대, 축소 이벤트 등록하기
     * 20. 중심좌표 변경 이벤트 등록하기
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/totalFunctionMap2")
    public String totalFunctionMap2(Model model) throws Exception {
        model.addAttribute("title", "2. 지도 기능 전부 때려박기.");
        log.info("model :: {}", model);
        return "map/totalFunctionMap2";
    }

}
