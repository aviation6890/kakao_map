package com.kakao.api.map.app.v1.overray.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class OverlayKakaoApiExampleController {

    /**
     * 1.  마커 생성하기                         /v1/overlay/createMarker
     * 2.  드래그 가능한 마커 생성하기            /v1/overlay/createCanBeDragMarker
     * 3.  다른 이미지로 마커 생성하기            /v1/overlay/createOtherImageMarker
     * 4.  인포윈도우 생성하기                    /v1/overlay/createInfoWindow
     * 5.  마커에 인포윈도우 표시하기             /v1/overlay/showInfoWindowOnTheMarker
     * 6.  마커에 클릭 이벤트 등록하기            /v1/overlay/registerClickEventOnTheMarker
     * 7.  마커에 마우스 이벤트 등록하기          /v1/overlay/registerMouseEventOnTheMarker
     * 8.  draggable 마커 이벤트 적용하기        /v1/overlay/applyDraggableMarkerEvent
     * 9.  geolocation으로 마커 표시하기         /v1/overlay/showGeoloacationOnTheMarker
     * 10. 여러개 마커 표시하기                  /v1/overlay/showSeveralMarker
     * 11. 여러개 마커 제어하기                  /v1/overlay/controlSeveralMarker
     * 12. 여러개 마커에 이벤트 등록하기1         /v1/overlay/registerSeveralMarkerAddEvent1
     * 13. 여러개 마커에 이벤트 등록하기2         /v1/overlay/registerSeveralMarkerAddEvent2
     * 14. 다양한 이미지 마커 표시하기            /v1/overlay/showVariousImageOnTheMarker
     * 15. 원, 선, 사각형, 다각형 표시하기        /v1/overlay/showSeveralModel
     * 16. 선의 거리 계산하기                    /v1/overlay/calculateDistanceOfLine
     * 17. 다각형의 면적 계산하기                 /v1/overlay/calculateAreaOfPolygon
     * 18. 다각형에 이벤트 등록하기1              /v1/overlay/registerPolygonEvent1
     * 19. 다각형에 이벤트 등록하기2              /v1/overlay/registerPolygonEvent2
     * 20. 원의 반경 계산하기                    /v1/overlay/calculateCircleRadius
     * 21. 커스텀 오버레이 생성하기1              /v1/overlay/createCustomOverlay1
     * 22. 커스텀 오버레이 생성하기2              /v1/overlay/createCustomOverlay2
     * 23. 닫기가 가능한 커스텀 오버레이          /v1/overlay/closeCanBeCustomOverlay
     * 24. 이미지 마커와 커스텀 오버레이          /v1/overlay/imageMarkerCustomOverlay
     * 25. 커스텀오버레이를 드래그 하기           /v1/overlay/draggableCustomOverlay
     * 26. 지도 영역 밖의 마커위치 추적하기       /v1/overlay/trackingMarkerOutOfMap
     * 27. 구멍난 다각형 만들기                  /v1/overlay/createPerforatedPolygon
     */

    /**
     * 1.  마커 생성하기
     * <p>
     * 지도에 올라가는 핀 모양의 이미지를 마커라고 부릅니다. 아래 예제는 지도 위에 마커를 표시하는 기본 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/createMarker")
    public String getCreateMarker(Model model) throws Exception {
        model.addAttribute("title", "1.마커 생성하기");
        log.info("model :: {}", model);
        return "overlay/createMarker";
    }

    /**
     * 2.  드래그 가능한 마커 생성하기
     * <p>
     * 지도에 올라가는 핀 모양의 이미지를 마커라고 부릅니다.
     * 아래 예제는 지도 위에 마커를 표시하는 기본 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/createCanBeDragMarker")
    public String createCanBeDragMarker(Model model) throws Exception {
        model.addAttribute("title", "2.  드래그 가능한 마커 생성하기");
        log.info("model :: {}", model);
        return "overlay/createCanBeDragMarker";
    }

    /**
     * 3.  다른 이미지로 마커 생성하기
     * <p>
     * 원하는 이미지를 사용해서 더 멋진 마커를 만들 수 있습니다.
     * 아래 예제는 마커로 사용할 이미지의 크기나 꼭지점 위치 등을 지정하여 기본 마커 대신 이용하는 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/createOtherImageMarker")
    public String createOtherImageMarker(Model model) throws Exception {
        model.addAttribute("title", "3.  다른 이미지로 마커 생성하기");
        log.info("model :: {}", model);
        return "overlay/createOtherImageMarker";
    }

    /**
     * 4. 인포위도우 생성하기.
     * <p>
     * 텍스트를 올릴 수 있는 말풍선 모양의 이미지를 인포윈도우라고 부릅니다.
     * 아래 예제는 삭제 버튼을 포함한 인포윈도우를 지도에 표시하는 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/createInfoWindow")
    public String createInfoWindow(Model model) throws Exception {
        model.addAttribute("title", "4. 인포위도우 생성하기.");
        log.info("model :: {}", model);
        return "overlay/createInfoWindow";
    }


    /**
     * 5. 마커에 인포윈도우 표시하기
     * <p>
     * 아래 예제는 삭제 버튼을 포함한 인포윈도우를 지도에 표시하는 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/showInfoWindowOnTheMarker")
    public String showInfoWindowOnTheMarker(Model model) throws Exception {
        model.addAttribute("title", "5.  마커에 인포윈도우 표시하기");
        log.info("model :: {}", model);
        return "overlay/showInfoWindowOnTheMarker";
    }

    /**
     * 6.  마커에 클릭 이벤트 등록하기
     * <p>
     * 마커를 마우스로 클릭했을때 click 이벤트가 발생합니다.
     * 이 예제에서는 마커를 클릭했을 때 마커 위에 인포윈도우를 표시하고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/registerClickEventOnTheMarker")
    public String registerClickEventOnTheMarker(Model model) throws Exception {
        model.addAttribute("title", "6. 마커에 클릭 이벤트 등록하기");
        log.info("model :: {}", model);
        return "overlay/registerClickEventOnTheMarker";
    }

    /**
     * 7.  마커에 마우스 이벤트 등록하기.
     * <p>
     * 마커에 마우스 커서를 올렸을때 mouseover 이벤트가,
     * 마우스 커서를 내리면 mouseout 이벤트가 발생합니다.
     * 이 예제에서는 마커에 마우스 커서를 올리고 내릴때 인포윈도우를 표시하거나 제거하고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/registerMouseEventOnTheMarker")
    public String registerMouseEventOnTheMarker(Model model) throws Exception {
        model.addAttribute("title", "7.  마커에 마우스 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "overlay/registerMouseEventOnTheMarker";
    }

    /**
     * 8. draggable 마커 이벤트 적용하기        /v1/overlay/applyDraggableMarkerEvent
     * <p>
     * 마커에 마우스 커서를 올렸을때 mouseover 이벤트가,
     * 마우스 커서를 내리면 mouseout 이벤트가 발생합니다.
     * 이 예제에서는 마커에 마우스 커서를 올리고 내릴때 인포윈도우를 표시하거나 제거하고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/applyDraggableMarkerEvent")
    public String applyDraggableMarkerEvent(Model model) throws Exception {
        model.addAttribute("title", "8. draggable 마커 이벤트 적용하기");
        log.info("model :: {}", model);
        return "overlay/applyDraggableMarkerEvent";
    }

    /**
     * 9.  geolocation으로 마커 표시하기         /v1/overlay/showGeoloacationOnTheMarker
     * <p>
     * HTML5 GeoLocation을 이용해 접속위치를 얻어오고 접속위치에 마커와 인포윈도우를 표시합니다.
     * Chrome 브라우저는 https 환경에서만 geolocation을 지원합니다. 참고해주세요.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/showGeoloacationOnTheMarker")
    public String showGeoloacationOnTheMarker(Model model) throws Exception {
        model.addAttribute("title", "9.  geolocation으로 마커 표시하기 ");
        log.info("model :: {}", model);
        return "overlay/showGeoloacationOnTheMarker";
    }

    /**
     * 10. 여러개 마커 표시하기         /v1/overlay/showSeveralMarker
     * <p>
     * 지도 위에 마커 이미지를 사용하여 여러개의 마커를 표시합니다
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/showSeveralMarker")
    public String showSeveralMarker(Model model) throws Exception {
        model.addAttribute("title", "10. 여러개 마커 표시하기 ");
        log.info("model :: {}", model);
        return "overlay/showSeveralMarker";
    }


}
