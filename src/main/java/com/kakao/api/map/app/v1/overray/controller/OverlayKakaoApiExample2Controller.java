package com.kakao.api.map.app.v1.overray.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class OverlayKakaoApiExample2Controller {

    /**
     * 11. 여러개 마커 제어하기                  /v1/overlay/controlSeveralMarker
     * 12. 여러개 마커에 이벤트 등록하기1         /v1/overlay/registerSeveralMarkerAddEvent1
     * 13. 여러개 마커에 이벤트 등록하기2         /v1/overlay/registerSeveralMarkerAddEvent2
     * 14. 다양한 이미지 마커 표시하기            /v1/overlay/showVariousImageOnTheMarker
     * 15. 원, 선, 사각형, 다각형 표시하기        /v1/overlay/showSeveralModel
     * 16. 선의 거리 계산하기                    /v1/overlay/calculateDistanceOfLine
     * 17. 다각형의 면적 계산하기                 /v1/overlay/calculateAreaOfPolygon
     * 18. 다각형에 이벤트 등록하기1              /v1/overlay/registerPolygonEvent1
     * 19. 다각형에 이벤트 등록하기2              /v1/overlay/registerPolygonEvent2
     * 20. 원의 반경 계산하기                    /v1/overlay/calculateCircleRadius
     */

    /**
     * 11. 여러개 마커 제어하기                  /v1/overlay/controlSeveralMarker
     * <p>
     * 지도를 클릭한 위치에 마커를 표시하고 지도에 표시되고 있는 마커들을 감추고 다시 지도 위에 표시합니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/controlSeveralMarker")
    public String controlSeveralMarker(Model model) throws Exception {
        model.addAttribute("title", "11. 여러개 마커 제어하기  ");
        log.info("model :: {}", model);
        return "overlay/controlSeveralMarker";
    }

    /**
     * 12. 여러개 마커에 이벤트 등록하기1         /v1/overlay/registerSeveralMarkerAddEvent1
     * <p>
     * 여러개의 마커에 마우스 이벤트를 등록합니다.
     * 마커에 마우스오버하면 인포윈도우에 마커의 타이틀을 표시하고 마우스아웃하면 인포윈도우를 닫습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/registerSeveralMarkerAddEvent1")
    public String registerSeveralMarkerAddEvent1(Model model) throws Exception {
        model.addAttribute("title", "12. 여러개 마커에 이벤트 등록하기1");
        log.info("model :: {}", model);
        return "overlay/registerSeveralMarkerAddEvent1";
    }

    /**
     * 13. 여러개 마커에 이벤트 등록하기2         /v1/overlay/registerSeveralMarkerAddEvent2
     * <p>
     * 이 샘플에서는 여러개의 마커에 마우스 이벤트를 등록하고 이벤트가 발생할 때 마다 마커에 다른이미지가 표시되도록 합니다.
     * 마우스 오버시 조금 큰 마커 이미지로 변경되고 마우스 아웃시 처음 이미지로 변경합니다.
     * 마우스 클릭시에는 다른 색깔의 마커 이미지로 변경하고 마우스 오버, 아웃 이벤트가 발생해도 이미지가 바뀌지 않습니다.
     * 다른 마커를 클릭하면 클릭됐었던 마커는 다시 기본 마커로 이미지를 변경합니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/registerSeveralMarkerAddEvent2")
    public String registerSeveralMarkerAddEvent2(Model model) throws Exception {
        model.addAttribute("title", "12. 여러개 마커에 이벤트 등록하기2");
        log.info("model :: {}", model);
        return "overlay/registerSeveralMarkerAddEvent2";
    }

    /**
     * 14. 다양한 이미지 마커 표시하기            /v1/overlay/showVariousImageOnTheMarker
     * <p>
     * 텍스트를 올릴 수 있는 말풍선 모양의 이미지를 인포윈도우라고 부릅니다.
     * 아래 예제는 삭제 버튼을 포함한 인포윈도우를 지도에 표시하는 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/showVariousImageOnTheMarker")
    public String showVariousImageOnTheMarker(Model model) throws Exception {
        model.addAttribute("title", "14. 다양한 이미지 마커 표시하기.");
        log.info("model :: {}", model);
        return "overlay/showVariousImageOnTheMarker";
    }


    /**
     * 15. 원, 선, 사각형, 다각형 표시하기        /v1/overlay/showSeveralModel
     * <p>
     * 아래 예제는 삭제 버튼을 포함한 인포윈도우를 지도에 표시하는 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/showSeveralModel")
    public String showSeveralModel(Model model) throws Exception {
        model.addAttribute("title", "15. 원, 선, 사각형, 다각형 표시하기");
        log.info("model :: {}", model);
        return "overlay/showSeveralModel";
    }

    /**
     * 16. 선의 거리 계산하기                    /v1/overlay/calculateDistanceOfLine
     * <p>
     * 마커를 마우스로 클릭했을때 click 이벤트가 발생합니다.
     * 이 예제에서는 마커를 클릭했을 때 마커 위에 인포윈도우를 표시하고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/calculateDistanceOfLine")
    public String calculateDistanceOfLine(Model model) throws Exception {
        model.addAttribute("title", "16. 선의 거리 계산하기 ");
        log.info("model :: {}", model);
        return "overlay/calculateDistanceOfLine";
    }

    /**
     * 17. 다각형의 면적 계산하기                 /v1/overlay/calculateAreaOfPolygon
     * <p>
     * 마커에 마우스 커서를 올렸을때 mouseover 이벤트가,
     * 마우스 커서를 내리면 mouseout 이벤트가 발생합니다.
     * 이 예제에서는 마커에 마우스 커서를 올리고 내릴때 인포윈도우를 표시하거나 제거하고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/calculateAreaOfPolygon")
    public String calculateAreaOfPolygon(Model model) throws Exception {
        model.addAttribute("title", "17. 다각형의 면적 계산하기");
        log.info("model :: {}", model);
        return "overlay/calculateAreaOfPolygon";
    }

    /**
     * 18. 다각형에 이벤트 등록하기1              /v1/overlay/registerPolygonEvent1
     * <p>
     * 마커에 마우스 커서를 올렸을때 mouseover 이벤트가,
     * 마우스 커서를 내리면 mouseout 이벤트가 발생합니다.
     * 이 예제에서는 마커에 마우스 커서를 올리고 내릴때 인포윈도우를 표시하거나 제거하고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/registerPolygonEvent1")
    public String registerPolygonEvent1(Model model) throws Exception {
        model.addAttribute("title", "18. 다각형에 이벤트 등록하기1 ");
        log.info("model :: {}", model);
        return "overlay/registerPolygonEvent1";
    }

    /**
     * 19. 다각형에 이벤트 등록하기2                 /v1/overlay/registerPolygonEvent2
     * <p>
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/registerPolygonEvent2")
    public String registerPolygonEvent2(Model model) throws Exception {
        model.addAttribute("title", "17. 다각형의 면적 계산하기");
        log.info("model :: {}", model);
        return "overlay/registerPolygonEvent2";
    }

    /**
     * 20. 원의 반경 계산하기               /v1/overlay/calculateCircleRadius
     * <p>
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/overlay/calculateCircleRadius")
    public String calculateCircleRadius(Model model) throws Exception {
        model.addAttribute("title", "20. 원의 반경 계산하기  ");
        log.info("model :: {}", model);
        return "overlay/calculateCircleRadius";
    }
}
