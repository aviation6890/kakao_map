package com.kakao.api.map.app.v1.map.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
//@PropertySource("classpath:key.properties")
public class MapKakaoApiExampleController {

    //@Value("${javascript}")
    //private String kakakoApiJavaScript;






    /**
     * 1. 지도생성하기.
     * <p>
     * 지도를 생성하는 가장 기본적인 예제입니다.
     *
     * @throws Exception
     */
    @GetMapping("/v1/createMap")
    public String createMap(Model model) throws Exception {
        model.addAttribute("title", "1.지도 생성하기");
        // model.addAttribute("API_KEY", kakakoApiJavaScript);
        log.info("model :: {}", model);
        return "map/createMap";
    }

    /**
     * 2. 지도 이동시키기.
     * <p>
     * 지도를 이동시킵니다.
     * 지도 객체의 메소드를 통해 지도를 원하는 좌표로 이동시킬 수 있습니다.
     * 또, 지도가 표시되고 있는 영역크기를 벗어나지 않는 거리라면 애니메이션 효과처럼 지도를 부드럽게 이동시킬 수도 있습니다.
     *
     * @throws Exception
     */
    @GetMapping("/v1/moveMap")
    public String moveMap(Model model) throws Exception {
        model.addAttribute("title", "2.지도 이동시키기");
        log.info("model :: {}", model);
        return "map/moveMap";
    }

    /**
     * 3. 지도 레벨 바꾸기.
     * <p>
     * 지도 레벨을 지도 객체 메소드를 호출해서 변경합니다.
     *
     * @throws Exception
     */

    @GetMapping("/v1/changeLevelMap")
    public String changeLevelMap(Model model) throws Exception {
        model.addAttribute("title", "3. 지도 레벨 바꾸기.");
        log.info("model :: {}", model);
        return "map/changeLevelMap";
    }

    /**
     * 4. 지도 정보 얻어오기
     * <p>
     * 지도 레벨, 중심좌표, 지도 타입, 지도 영역정보를 얻어와 표출합니다.
     *
     * @throws Exception
     */
    @GetMapping("/v1/getInfoMap")
    public String getInfoMap(Model model) throws Exception {
        model.addAttribute("title", "4. 지도 정보 얻어오기");
        log.info("model :: {}", model);
        return "map/getInfoMap";
    }

    /**
     * 5. 지도 컨트롤 올리기
     * <p>
     * 일반 지도와 스카이뷰로 지도 타입을 전환할 수 있는 버튼과 지도 확대, 축소를 제어할 수 있는 도구를 쉽게 지도 위에 올릴 수 있습니다.
     * 각각 지도 타입 컨트롤, 지도 줌 컨트롤이라고 부르며,
     * 아래 예제와 같이 지도 위에 표시될 위치를 지정할 수도 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */

    @GetMapping("/v1/mapControlMap")
    public String mapControlMap(Model model) throws Exception {
        model.addAttribute("title", "5. 지도 컨트롤 올리기");
        log.info("model :: {}", model);
        return "map/mapControlMap";
    }

    /**
     * 6. 지도에 사용자 컨트롤 올리기
     * <p>
     * 기본으로 제공하는 지도타입 컨트롤과 줌 컨트롤을 원하는 스타일로 직접 만들 수 있습니다.
     * 컨트롤의 색이나 크기 등을 변경해야 할때 유용하겠지요.
     * 아래 예제의 Javascript+HTML 코드를 참고해보세요.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/userControlMap")
    public String userControlMap(Model model) throws Exception {
        model.addAttribute("title", "6. 지도에 사용자 컨트롤 올리기");
        log.info("model :: {}", model);
        return "map/userControlMap";
    }

    /**
     * 7. 지도 이동 막기.
     * <p>
     * 마우스 드래그로 지도를 이동시키는 기능을 막습니다.
     * 모바일 기기에서 터치스크롤시 지도가 이동되는 것을 막고싶거나
     * 지도가 이동되면 안되는 경우 등 상황에 따라 지도의 이동 기능을 제어할 수 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/blockMoveMap")
    public String blockMoveMap(Model model) throws Exception {
        model.addAttribute("title", "7. 지도 이동 막기.");
        log.info("model :: {}", model);
        return "map/blockMoveMap";
    }

    /**
     * 8. 지도 확대 축소 막기.
     * <p>
     * 마우스 휠이나 멀티터치로 지도 확대, 축소 기능을 막습니다.
     * 상황에 따라 지도 확대, 축소 기능을 제어할 수 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/blockZoomInOutMap")
    public String blockZoomInOutMap(Model model) throws Exception {
        model.addAttribute("title", "8. 지도 확대 축소 막기.");
        log.info("model :: {}", model);
        return "map/blockZoomInOutMap";
    }

    /**
     * 9. 지도에 교통정보 표시하기.
     * <p>
     * 지도에 현재 실시간 교통 정보가 컬러로 표시되어 있는 라인을 올릴 수 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/getTrafficMap")
    public String getTrafficMap(Model model) throws Exception {
        model.addAttribute("title", "9. 지도에 교통정보 표시하기.");
        log.info("model :: {}", model);
        return "map/getTrafficMap";
    }

    /**
     * 10. 지도에 로드뷰 표시하기.
     * <p>
     * 지도 위에 로드뷰 정보가 있는 도로를 표시할 수 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */

    @GetMapping("/v1/getLoadViewMap")
    public String getLoadViewMap(Model model) throws Exception {
        model.addAttribute("title", "10. 지도에 로드뷰 표시하기.");
        log.info("model :: {}", model);
        return "map/getLoadViewMap";
    }

    /**
     * 11. 지형도 표시하기.
     * <p>
     * 지도 위에 지형 정보를 알 수 있는 지형도를 표시할 수 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/topographicMap")
    public String topographicMap(Model model) throws Exception {
        model.addAttribute("title", "11. 지형도 표시하기.");
        log.info("model :: {}", model);
        return "map/topographicMap";
    }

    /**
     * 12. 지도 바꾸기 1( button)
     * 교통정보, 로드뷰 도로정보, 지형도 정보를 버튼 클릭에 따라 지도 타입을 변경합니다.
     * <p>
     * 교통정보 / 로드뷰 / 지형도 / 지적편집도.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/changeTypeMap1")
    public String changeTypeMap1(Model model) throws Exception {
        model.addAttribute("title", "12. 지도 바꾸기 1( button)");
        log.info("model :: {}", model);
        return "map/changeTypeMap1";
    }

    /**
     * 13. 지도 바꾸기 2(checkbox)
     * 지형정보, 교통정보, 자전거도로 정보를 체크박스 선택에 따라 지도타입을 겹쳐보이게 표시합니다.
     * <p>
     * 교통정보 / 로드뷰 / 지형도 / 지적편집도.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/changeTymeMap2")
    public String changeTymeMap2(Model model) throws Exception {
        model.addAttribute("title", "13. 지도 바꾸기 2(checkbox)");
        log.info("model :: {}", model);
        return "map/changeTypeMap2";
    }

    /**
     * 14. 지도 범위 재설정 하기.
     * <p>
     * 지도 범위를 재설정합니다. 어떤 좌표나 마커들이 지도에 모두 보여야 할 때
     * 좌표들의 정보를 갖는 LatLngBounds를 사용하여 좌표들이 모두 보이게
     * 지도의 중심좌표와 레벨을 재설정 할 수 있습니다.
     * </p>
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/relocateRangeMap")
    public String relocateRangeMap(Model model) throws Exception {
        model.addAttribute("title", "14. 지도 범위 재설정 하기.");
        log.info("model :: {}", model);
        return "map/relocateRangeMap";
    }


    /**
     * 15. 지도 영역 크기 동적 변경하기.
     * <p>
     * 지도 객체는 생성될 때 지도 div 크기에 따라 픽셀과 좌표정보를 설정하여 가지고 있습니다.
     * 이 정보로 지도 객체는 지도표시, 마커 표시, 확대, 축소, 이동 등의 좌표 계산 등 지도 표시에
     * 필요한 여러가지 연산을 수행하는데 이때 지도 div의 크기가 변경이 되면 지도객체가 가지고 있는 픽셀과
     * 좌표정보가 div를 표시하는 크기와 달라지기 때문에 지도가 정상적으로 표출되지 않을 수도 있습니다.
     * 그래서 크기를 변경한 이후에는 반드시 relayout 함수를 호출하여 픽셀과 좌표정보를 새로 설정해주어야합니다.
     * window의 resize 이벤트에 의한 크기변경은 map.relayout 함수가 자동으로 호출됩니다.
     *
     * @param model
     * @return
     * @throws Exception
     */

    @GetMapping("/v1/dynamicChangeMap")
    public String dynamicChangeMap(Model model) throws Exception {
        model.addAttribute("title", "15. 지도 영역 크기 동적 변경하기.");
        log.info("model :: {}", model);
        return "map/dynamicChangeMap";
    }


    /**
     * 16. 클릭 이벤트 등록하기.
     * <p>
     * 지도를 마우스로 클릭했을때 click 이벤트가 발생합니다.
     * 이 예제에서는 지도를 클릭했을 때 지도 아래쪽에 해당 위치의 좌표를 뿌려주고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/clickRegEvent")
    public String clickRegEvent(Model model) throws Exception {
        model.addAttribute("title", "15. 클릭 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "map/clickRegEvent";
    }

    /**
     * 17. 클릭한 위치에 마커 표시하기.
     * <p>
     * 지도를 마우스로 클릭했을때 click 이벤트가 발생합니다.
     * 이 예제는 지도를 클릭했을 때 해당 위치에 마커를 올리는 예제입니다.
     * 이때 마커는 1개만 생성하여 클릭한 위치로 옮겨주고 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/clickMarkerRegMap")
    public String clickMarkerRegMap(Model model) throws Exception {
        model.addAttribute("title", "16. 클릭한 위치에 마커 표시하기.");
        log.info("model :: {}", model);
        return "map/clickMarkerRegMap";
    }

    /**
     * 18. 이동 이벤트 등록하기.
     * <p>
     * 지도를 마우스로 이동시킬 경우에 dragend 이벤트가 발생합니다.
     * 아래 예제는 마우스로 지도 이동이 완료된 시점의 중심 좌표를 표시하는 예제입니다.
     *
     * @param model
     * @return
     * @throws Exception
     */

    @GetMapping("/v1/moveEventReg")
    public String moveEventReg(Model model) throws Exception {
        model.addAttribute("title", "18. 이동 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "map/moveEventReg";
    }

    /**
     * 19. 확대, 축소 이벤트 등록하기.
     * <p>
     * 지도를 확대, 축소하거나, 지도 API의 특정 메소드를 호출하여 지도 레벨이 변경되는 경우에는 zoom_changed 이벤트가 발생합니다.
     * 이 예제에서는 지도 레벨이 변경되었을 때 지도 아래쪽에 변경된 지도 레벨을 뿌려주고 있습니다.
     * 줌 컨트롤을 이용하거나 마우스 스크롤을 이용하여 지도를 확대, 축소해보세요.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/zoomInOutEventReg")
    public String zoomInOutEventReg(Model model) throws Exception {
        model.addAttribute("title", "19. 확대, 축소 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "map/zoomInOutEventReg";
    }

    /**
     * 20. 중심좌표 변경 이벤트 등록하기.
     * <p>
     * 지도를 이동하거나, 지도 API의 특정 메소드를 호출하여 지도의 중심좌표가 변경되었을 때
     * center_changed 이벤트가 발생합니다.
     * 이 예제에서는 지도의 중심좌표가 변경되었을때 지도 아래쪽에 변경된 중심좌표 및 지도 레벨을 뿌려주고 있습니다.
     * 지도를 움직여서 확인해보세요.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/centerLocationChangeEventReg")
    public String centerLocationChangeEventReg(Model model) throws Exception {
        model.addAttribute("title", "20. 중심좌표 변경 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "map/centerLocationChangeEventReg";
    }

    /**
     * 21. 영역 변경 이벤트 등록하기.
     * <p>
     * 지도를 이동 또는 확대/축소 하거나,
     * 지도 API의 특정 메소드를 호출하여 지도 영역이 변경되는 경우에는 bounds_changed 이벤트가 발생합니다.
     * 이 예제에서는 지도 영역이 변경되었을 때 지도 아래쪽에 변경된 지도 영역의 좌표를 뿌려주고 있습니다.
     * 지도를 움직여서 확인해보세요.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/domainChangeEventReg")
    public String domainChangeEventReg(Model model) throws Exception {
        model.addAttribute("title", "21. 영역 변경 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "map/domainChangeEventReg";
    }

    /**
     * 22. 타일로드 이벤트 등록하기.
     * <p>
     * 지도를 확대, 축소하거나 지도를 움직인 이후에 타일 이미지가 모두 완료되면 tilesloaded 이벤트가 발생합니다.
     * 이 예제에서는 타일 이미지가 모두 로드 된 이후에 tilesloaded 이벤트가 발생하면 마커의 위치를 지도중심으로 설정합니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/tileLoadEventReg")
    public String tileLoadEventReg(Model model) throws Exception {
        model.addAttribute("title", "22. 타일로드 이벤트 등록하기.");
        log.info("model :: {}", model);
        return "map/tileLoadEventReg";
    }

    /**
     * 23. 커스텀 타일셋 1
     * <p>
     * 직접 만든 타일 이미지로 타일셋을 만들어 사용할 수 있습니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/customTileSet1")
    public String customTileSet1(Model model) throws Exception {
        model.addAttribute("title", "23. 커스텀 타일셋 1");
        log.info("model :: {}", model);
        return "map/customTileSet1";
    }

    /**
     * 23. 커스텀 타일셋 2
     * <p>
     * Tileset 객체를 생성할 때 getTile 함수를 파라미터로 입력하면 지도 위에 이미지 URL이 아닌 다른 HTML Element를 타일로 표시할 수 있습니다.
     * 이 예제에서는 Div Element에 지도 타일 좌표와 레벨을 표시합니다.
     *
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("/v1/customTileSet2")
    public String customTileSet2(Model model) throws Exception {
        model.addAttribute("title", "23. 커스텀 타일셋 2");
        log.info("model :: {}", model);
        return "map/customTileSet2";
    }
}
